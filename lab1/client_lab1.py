import socket
from time import *
from pynput import keyboard
import _thread

# CONFIGURATION PARAMETERS
IP_ADDRESS = "192.168.1.101" 	# SET THIS TO THE RASPBERRY PI's IP ADDRESS
CONTROLLER_PORT = 5001
TIMEOUT = 10				# If its unable to connect after 10 seconds, give up. 

class globalStuff():
    SPEED = 200
    lastCommand = ''
    direction = 'right'
    numPoly = 4

    """
    @author Maximilian Puglielli

    PARAM:	n/a
    RETURN:	n/a

    FUNCTION: This function sends "set_song()" & "play_song()" commands to the roomba to play the first
    six bars of the super mario theme song.

    NOTE: This function uses functions from the "socket" and "time" libraries, so they must be imported
    for this function to work properly. Also, this function uses the "sleep()" function inbetween sending
    the various "play_song()" commands, such that the roomba will first finish a song before starting to
    play the next one. There is an extra "sleep()" command at the end of the function such that the roomba
    will finish playing the song before executing any other commands, which could cause problems if implemented
    improperly.
    """
    @staticmethod
    def super_mario():
            # Initialize songs 0 through 3
            sock.sendall("a set_song(0, [(76,11), (76,22), (76,22), (72,11), (76,22), (79,22)])".encode())
            print(sock.recv(128).decode())
            sock.sendall("a set_song(1, [(67,43), (72,33), (67,22), (64,22), (69,22), (71,11)])".encode())
            print(sock.recv(128).decode())
            sock.sendall("a set_song(2, [(70,11), (69,22), (67,14), (76,14), (79,14), (81,22)])".encode())
            print(sock.recv(128).decode())
            sock.sendall("a set_song(3, [(77,11), (79,22), (76,22), (72,11), (74,11), (71,22)])".encode())
            print(sock.recv(128).decode())

            # Play 0 through 3 with the correct wait times after each song
            sock.sendall("a play_song(0)".encode())
            print(sock.recv(128).decode())

            sleep(2)

            sock.sendall("a play_song(1)".encode())
            print(sock.recv(128).decode())

            sleep(2.5)

            sock.sendall("a play_song(2)".encode())
            print(sock.recv(128).decode())

            sleep(1.52)

            sock.sendall("a play_song(3)".encode())
            print(sock.recv(128).decode())

            sleep(1.56)

    @staticmethod
    def polygon(num_sides=4,direction='left'):
        for i in range(num_sides):
            sock.sendall('a spin_{}(300)'.format(direction).encode())
            print(sock.recv(128).decode())
            sleep(.6)
            sock.sendall('a drive_straight(300)'.encode())
            print(sock.recv(128).decode())
            sleep(.7)

    @staticmethod
    def playMario():
        _thread.start_new_thread(globalStuff.super_mario,('',))



                                        # Want this to be a while so robot can initialize.
# connect to the motorcontroller
sock = socket.create_connection( (IP_ADDRESS, CONTROLLER_PORT), TIMEOUT)

""" The t command tests the robot.  It should beep after connecting, move forward
slightly, then beep on a sucessful disconnect."""
#sock.sendall("t /dev/tty.usbserial-DA01NYDH")			# send a command
#print(sock.recv(128))        # always recieve to confirm that your command was processed

""" The i command will initialize the robot.  It enters the create into FULL mode which
 means it can drive off tables and over steps: be careful!"""
sock.sendall("i /dev/ttyUSB0".encode())
print(sock.recv(128).decode())

"""
    Arbitrary commands look like this
        a *
    Whatever text is given where the * is, is given to the Create API in the form
        result = robot.*
    then any result will be send back.  If there is no result the command will be echoed.

    You can see the possible commands here:
    https://bitbucket.org/lemoneer/irobot

    You may wish to extend the control_server.py on the raspberry pi.
"""


# polygon(8,'right')

# ======remote control code=================

def on_press(key):
    try:
        if key.char == 'w':
            globalStuff.lastCommand = 'a drive_straight({})'
        elif key.char == 's':
            globalStuff.lastCommand = 'a drive_straight(-{})'
        elif key.char == 'a':
            globalStuff.lastCommand = 'a spin_left({})'
        elif key.char == 'd':
            globalStuff.lastCommand = 'a spin_right({})'
        elif key.char == 'p':
            globalStuff.polygon(globalStuff.numPoly,globalStuff.direction)
        elif key.char == 'q':
            sock.sendall('a drive_direct(500,200)'.encode())
            print(sock.recv(128).decode())
            return
        elif key.char == 'e':
            sock.sendall('a drive_direct(200,500)'.encode())
            print(sock.recv(128).decode())
            return
        elif key.char == '-':
            globalStuff.numPoly = globalStuff.numPoly - 1
            print("num sides: {}".format(globalStuff.numPoly))
        elif key.char == '=':
            globalStuff.numPoly = globalStuff.numPoly + 1
            print("num sides: {}".format(globalStuff.numPoly))
        elif key.char == 'o':
            if globalStuff.direction == 'right':
                globalStuff.direction = 'left'
            else:
                globalStuff.direction = 'right'
        elif key.char == 'x':
            return False

        sock.sendall(globalStuff.lastCommand.format(globalStuff.SPEED).encode())
        print(sock.recv(128).decode())

    except AttributeError:
        if key == keyboard.Key.space:
            globalStuff.lastCommand = 'a drive_straight(0)'
            sock.sendall(globalStuff.lastCommand.encode())
            print(sock.recv(128).decode())
        elif key == keyboard.Key.down:
            newSpeed = globalStuff.SPEED -50
            if not newSpeed < 0:
                globalStuff.SPEED = newSpeed
            sock.sendall(globalStuff.lastCommand.format(globalStuff.SPEED).encode())
            print(sock.recv(128).decode())
        elif key == keyboard.Key.up:
            newSpeed = globalStuff.SPEED + 50
            if not newSpeed > 500:
                globalStuff.SPEED = newSpeed
            sock.sendall(globalStuff.lastCommand.format(globalStuff.SPEED).encode())
            print(sock.recv(128).decode())
        elif key == keyboard.Key.enter:
            globalStuff.super_mario()



with keyboard.Listener(on_press=on_press) as listener:
    listener.join()


sock.sendall("a distance".encode())
print("It has traveled this far: ",sock.recv(128).decode())


""" The c command stops the robot and disconnects.  The stop command will also reset
the Create's mode to a battery safe PASSIVE.  It is very important to use this command!"""
sock.sendall("c".encode())
print(sock.recv(128).decode())

sock.close()



